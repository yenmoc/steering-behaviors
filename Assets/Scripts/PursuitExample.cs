﻿using UnityEngine;

public class PursuitExample : MonoBehaviour
{
   // dự đoán (ước tính) trước target sẽ di chuyển trong vài giây tiếp theo để điều quỹ đạo di chuyển
   // tính trước điểm đến của target dựa vào vector vận tốc và hệ số t 
   
   public float mass = 15;
   public float maxVelocity = 3;
   public float maxForce = 15;

   private Vector3 _velocity = Vector3.zero;
   public PursuitTargetMovement target;

   public float t;
   private void Update()
   {
      var force = Steering.Pursuit(transform.position, target.transform.position, maxForce, target.velocity, t, mass);
      _velocity = Vector3.ClampMagnitude(_velocity + force, maxVelocity);
      transform.position += _velocity * Time.deltaTime;
   }
}
