﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalExample : MonoBehaviour
{
    // seek behavior làm cho object tiến về phía target khi đến đích force sẽ tiếp tục dựa trên các tính toán như cũ khiến cho object "bounce" xung quanh target
    // arrival ngăn object di chuyển qua target nó làm object chậm dần khi đến gần target và dừng lại khi đến đích
    // arrival bao gồm 2 giai đoạn
    //    + giai đoạn ở xa target ở giai đoạn này nó giống như seek di chuyển nhanh hết mức về phía target
    //    + giai đoạn khi ở gần target bên trong "slowing area" (vòng tròn có tâm là target bán kính r) khi object đi vào nó sẽ làm chậm lại cho đến khi dừng hẳn ở target


    public float mass = 15;
    public float maxVelocity = 3;
    public float maxForce = 15;
    public float radius;

    private Vector3 _velocity = Vector3.zero;
    public Transform target;

    private void Update()
    {
        var force = Steering.Arrival(transform.position, target.position, radius, maxForce, _velocity, mass);
        _velocity = Vector3.ClampMagnitude(_velocity + force, maxVelocity);
        transform.position += _velocity * Time.deltaTime;
    }
}