﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeExample : MonoBehaviour
{
    // Flee là hành vi object chạy trốn khỏi target
    // Flee = Seek * -1

    public float mass = 15;
    public float maxVelocity = 3;
    public float maxForce = 15;

    private Vector3 _velocity = Vector3.zero;
    public Transform target;

    private void Update()
    {
        var force = Steering.Flee(transform.position, target.position, maxForce, _velocity, mass);
        _velocity = Vector3.ClampMagnitude(_velocity + force, maxVelocity);
        transform.position += _velocity * Time.deltaTime;
    }
}