﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeExample : MonoBehaviour
{
    // một sự nâng cấp từ flee
    // object sẽ dự đoán trước vị trí trong tương lai của target nhờ vector vận tốc
    
    public float mass = 15;
    public float maxVelocity = 3;
    public float maxForce = 15;

    private Vector3 _velocity = Vector3.zero;
    public Transform target;

    private void Update()
    {
        var force = Steering.Evade(transform.position, target.position, maxForce, _velocity, mass);
        _velocity = Vector3.ClampMagnitude(_velocity + force, maxVelocity);
        transform.position += _velocity * Time.deltaTime;
    }
}
