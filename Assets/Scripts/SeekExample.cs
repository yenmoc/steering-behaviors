﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekExample : MonoBehaviour
{
    // Steering behavior là tính toán dựa trên vecter-base
    // tại một thời điểm thì object sẽ có (positon và vecter vận tốc bao gồm hướng và độ lớn vận tốc)

    //quy tắc 
    //1. không được thêm vào một vector vận tốc khi chưa normalize vector đó (điều này đảm bảo vector chỉ bao gồm hướng và độ lớn là 1 để có thể nhân với các hệ số đặc biệt khác sau này)
    //2. tốc độ không được lớn hơn tốc độ tối đa
    //3. lực được tổng hợp không được lớn lực tối đa
    //4. luôn có 1 biến velocity lưu hướng và vận tốc của object


    //Seek là hành vi một object luôn đuổi theo một target của nó

    public float mass = 15;
    public float maxVelocity = 3;
    public float maxForce = 15;

    private Vector3 _velocity = Vector3.zero;
    public Transform target;

    private void Update()
    {
        var force = Steering.Seek(transform.position, target.position, maxForce, _velocity, mass);
        _velocity = Vector3.ClampMagnitude(_velocity + force, maxVelocity);
        transform.position += _velocity * Time.deltaTime;
    }
}