﻿using UnityEngine;
using Random = UnityEngine.Random;

public class WanderExample : MonoBehaviour
{
    // ý tưởng về cơ bản là tạo ra "small random displacements" và áp dụng cho vector chỉ hướng hiện tại của object.
    // vì vector vận tốc của object xác định vị trí và khoảng cách nó có thể di chuyển được trong mỗi frame nên sự thay đổi của vector cũng làm thay đổi "route" của object

    // do sử dụng small displacement mỗi frame nên nó sẽ ngăn object thay đổi đột ngột 
    // cách tiếp cận này có thể thực hiện bằng nhiều cách khác nhau một trong số đó là sử dụng một vòng tròn phía trước object 
    // lực dịch chuyển (displacement force) có nguồn gốc tại tâm vòng tròn và giới hạn bởi bán kính vòng tròn 
    // bán kính và khoảng cách từ object đến vòng tròn càng lớn thì lực đẩy object trên mỗi frame càng lớn.


    // caculate circle position
    public float circleRadius = 1;
    public float turnChance = 0.05f;
    public float maxRadius = 5;

    public float mass = 15;
    public float maxSpeed = 3;
    public float maxForce = 15;

    private Vector3 _velocity;
    private Vector3 _wanderForce;
    private Vector3 target;


    private void Start()
    {
        _velocity = Random.onUnitSphere;
        Steering.Wander(transform.position, target, maxRadius, circleRadius, _velocity, turnChance, ref _wanderForce);
    }

    private void Update()
    {
        var desiredVelocity = Steering.Wander(transform.position, target, maxRadius, circleRadius, _velocity, turnChance, ref _wanderForce);
        desiredVelocity = desiredVelocity.normalized * maxSpeed;

        var steeringForce = desiredVelocity - _velocity;
        steeringForce = Vector3.ClampMagnitude(steeringForce, maxForce);
        steeringForce /= mass;

        _velocity = Vector3.ClampMagnitude(_velocity + steeringForce, maxSpeed);
        transform.position += _velocity * Time.deltaTime;
        transform.forward = _velocity.normalized;

        //Debug.DrawRay(transform.position, velocity.normalized * 2, Color.green);
        //Debug.DrawRay(transform.position, desiredVelocity.normalized * 2, Color.magenta);
    }
}